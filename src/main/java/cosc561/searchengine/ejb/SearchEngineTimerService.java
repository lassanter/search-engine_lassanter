/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cosc561.searchengine.ejb;

import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

/**
 *
 * @author jraymond
 */
@Startup
@Singleton
public class SearchEngineTimerService
{
    @Resource
    TimerService timerService;

    public void startTimer()
    {
        TimerConfig timerConfig = new TimerConfig();
        ScheduleExpression scheduleExpression = new ScheduleExpression();

//        Timer timer = timerService.createCalendarTimer();
    }

    @Timeout
    public void handleTimeout(Timer timer)
    {

    }
}
