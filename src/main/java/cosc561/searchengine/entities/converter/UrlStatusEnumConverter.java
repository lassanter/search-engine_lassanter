/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cosc561.searchengine.entities.converter;

import cosc561.searchengine.enums.UrlStatus;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;

/**
 *
 * @author jraymond
 */
@Converter(autoApply = true)
public class UrlStatusEnumConverter implements AttributeConverter<UrlStatus, PGobject>
{
    @Override
    public PGobject convertToDatabaseColumn(UrlStatus attribute)
    {
        try
        {
            PGobject pGobject = new PGobject();

            pGobject.setType("url_status");
            pGobject.setValue(attribute.name());

            return pGobject;
        }
        catch (SQLException ex)
        {
            Logger.getLogger(UrlStatusEnumConverter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public UrlStatus convertToEntityAttribute(PGobject dbData)
    {
        return UrlStatus.valueOf(UrlStatus.class, dbData.getValue());
    }
}
